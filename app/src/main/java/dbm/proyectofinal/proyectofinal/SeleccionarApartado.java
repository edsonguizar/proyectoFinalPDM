package dbm.proyectofinal.proyectofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SeleccionarApartado extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_apartado);
    }

    //Método para ir a la lista de outfits
    public void Lista_Outfits(View view){
        Intent listaOutfits = new Intent(this,ListaOutfitsActivity.class);
        startActivity(listaOutfits);
    }

    //Método para ir al guardarropa
    public void Guardarropa(View view){
        Intent Guardarropa = new Intent(this,GuardarropaActivity.class);
        startActivity(Guardarropa);
    }

    //Método para ir a las estadísticas
    public void Estadisticas(View view){
        Intent Estadisticas = new Intent(this,EstadisticasActivity.class);
        startActivity(Estadisticas);
    }

    //Método para ir al historial
    public void Historial(View view){
        Intent Historial = new Intent(this,HistorialActivity.class);
        startActivity(Historial);
    }
}
